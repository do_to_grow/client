package com.example.myapplication.network

import kotlinx.serialization.Serializable

@Serializable
data class UserData(
    val possibility: Boolean,
    val remender: Boolean,
    val frequency: Int
)
