package com.example.myapplication.network


import com.example.myapplication.network.Plant
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import retrofit2.Retrofit
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.Query

private const val BASE_URL



private val retrofit: Retrofit = Retrofit.Builder()
    .addConverterFactory(Json.asConverterFactory("application/json".toMediaType()))
    .baseUrl(BASE_URL)
    .build()

interface DreamApiService {
    @POST("createUser")
    suspend fun getUser(): Int

    @GET("plants")
    suspend fun getFlower(@Query("stages") stages: Int): List<Plant>

    @GET("profiles/getInfo/")
    suspend fun getInfo(@Query("id") id: Int): UserData

    @PATCH("profiles/updateDate/")
    suspend fun checkRemender(@Query("id") id: Int): Boolean

    @PATCH("profiles/updatePro/")
    suspend fun checkCode(@Query("id") id: Int, @Query("code") code: String): Boolean

    @PATCH("profiles/updateRemender/")
    suspend fun sendRemender(@Query("id") id: Int,
                             @Query("remender") remender: Boolean,
                             @Query("frequency") frequency: Int = 0): Response<Unit>

}

object DreamApi {
    val retrofitService: DreamApiService by lazy {
        retrofit.create(DreamApiService::class.java)
    }
}
