package com.example.myapplication.network

sealed interface LoadState {
    object Success : LoadState
    object Error : LoadState
    object Loading : LoadState
}