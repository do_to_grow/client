package com.example.myapplication.network

import kotlinx.serialization.Serializable

@Serializable
data class Plant(
    val src_frame: String
)
