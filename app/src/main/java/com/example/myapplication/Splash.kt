package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.myapplication.data.localdata
import com.example.myapplication.network.LoadState
import com.example.myapplication.entrance.dataLoad
import com.example.myapplication.main.HostList
import com.example.myapplication.registration.FirstStep


class Splash : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier.fillMaxSize()) {

                val load: dataLoad = viewModel()
                when (load.state) {
                    is LoadState.Loading -> {
                        load.load(this@Splash)
                        Image(painter = painterResource(R.drawable.front_gradient), null)
                        Log.i("FileManager", "показ картинки")
                    }
                    is LoadState.Success -> {
                        val navToStep = if (localdata.actualUserDream == null) {
                            Intent(this@Splash, FirstStep::class.java)
                        } else {
                            Log.i("FileManager", "переходим в активити")
                            Intent(this@Splash, HostList::class.java)
                        }
                        this@Splash.startActivity(navToStep)
                        this@Splash.finish()
                    }
                    is LoadState.Error -> {
                        Log.i("FileManager", "ошибочка")
                        this@Splash.finish()
                    }
                }
            }
        }
    }
}




