package com.example.myapplication.main

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.selection.selectable
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.example.myapplication.R
import com.example.myapplication.generalUnit.IconList
import com.example.myapplication.generalUnit.IconPlant

@Composable
fun bottomAppBar(
    isPlant:Boolean,
    onClickPlant: () -> Unit,
    onClickList: () -> Unit
) {
    val color1: Color = if (isPlant) MaterialTheme.colors.background else MaterialTheme.colors.onError
    val color2: Color = if (!isPlant) MaterialTheme.colors.background else MaterialTheme.colors.onError

    Row(horizontalArrangement = Arrangement.SpaceEvenly,
        modifier = Modifier.background(MaterialTheme.colors.primaryVariant)
            .fillMaxWidth()
            .padding(7.dp)
    ) {
            Column(verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.selectable(
                    selected = isPlant,
                    enabled = !isPlant,
                    onClick = onClickPlant)
                .weight(1f)
                ) {
                IconPlant(color1)
                Spacer(modifier = Modifier.height(4.dp))
                Text(
                    text = stringResource(R.string.plant_name_view),
                    style = MaterialTheme.typography.body2,
                    color = color1,
                    textAlign = TextAlign.Center,
                )
            }
            Column(verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
                        modifier = Modifier.selectable(
                                selected = !isPlant,
                                enabled = isPlant,
                                onClick = onClickList)
                            .weight(1f)
                    ) {
                IconList(color2)
                Spacer(modifier = Modifier.height(4.dp))
                Text(
                    text = stringResource(R.string.list_name_view),
                    style = MaterialTheme.typography.body2,
                    color = color2,
                    textAlign = TextAlign.Center,
                )
            }

    }
}

