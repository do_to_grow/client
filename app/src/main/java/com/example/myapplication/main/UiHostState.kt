package com.example.myapplication.main

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import com.example.myapplication.data.UserDream
import com.example.myapplication.data.localdata

data class UiHostState(
    val dream: UserDream = localdata.actualUserDream ?: UserDream(mutableStateOf("Потерялась мечта")),
    val drawerOpen: MutableState<Boolean> =  mutableStateOf(false),
    val remenderShow: MutableState<Boolean> =  mutableStateOf(false),
    val drawerGesturesEnabled: MutableState<Boolean>  =  mutableStateOf(true)
)
