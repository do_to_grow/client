package com.example.myapplication.main


import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.myapplication.entrance.fileManager
import com.example.myapplication.generalUnit.TextBotton
import com.example.myapplication.generalUnit.filledButton
import com.example.myapplication.list.DialogWindow
import com.example.myapplication.plant.Plant
import com.example.myapplication.profile.Profile
import com.example.myapplication.setting.Setting
import com.example.myapplication.ui.theme.MyApplicationTheme
import kotlinx.coroutines.launch


class HostList : ComponentActivity() {
    private lateinit var currentScreen: AppScreen
    private lateinit var navController: NavHostController




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApplicationTheme {
                navController = rememberNavController()
                // Get current back stack entry
                val backStackEntry by navController.currentBackStackEntryAsState()
                // Get the name of the current screen
                currentScreen = AppScreen.valueOf(
                    backStackEntry?.destination?.route ?: AppScreen.plant.name
                )
                val hostModel: HostViewModel = viewModel()
                val dreamUiState by hostModel.uiState.collectAsState()
                val contex = LocalContext.current


                DialogWindow(alertFlag = dreamUiState.remenderShow.value, onDismissRequest = {dreamUiState.remenderShow.value =  false}, header = "Ваша мотивация", description = dreamUiState.dream.motive.value,
                    buttons = {
                    filledButton(text = "СПАСИБО") {
                        dreamUiState.remenderShow.value =  false
                    }
                })

                val scaffoldState = rememberScaffoldState()
                val scope = rememberCoroutineScope()

//                val intent = (contex as HostList).intent
//                hostModel.firstFlag = intent.getIntExtra("first", 0) == 1 || plant.stagesFlower == 0

                Scaffold(
                    drawerBackgroundColor = MaterialTheme.colors.background,
                    drawerScrimColor = MaterialTheme.colors.surface,
                    scaffoldState = scaffoldState,
                    drawerShape = MaterialTheme.shapes.medium,
                    drawerGesturesEnabled = dreamUiState.drawerGesturesEnabled.value,
                    drawerContent= {
                        Row(
                            horizontalArrangement = Arrangement.Start,
                            modifier = Modifier
                                .background(MaterialTheme.colors.onBackground)
                                .fillMaxWidth()
                                .padding(vertical = 10.dp)
                        ) {
                            Spacer(modifier = Modifier.width(7.dp))
                            Text(
                                text = "Путь к мечте",
                                style = MaterialTheme.typography.h1,
                                color = MaterialTheme.colors.background,
                                textAlign = TextAlign.Center,
                                modifier = Modifier.padding(start = 14.dp)
                            )
                        }
                        TextBotton(
                            "Главная",
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(start = 14.dp)
                                .padding(vertical = 4.dp)
                        ) {
                            navController.navigate(AppScreen.plant.name)
                            scope.launch { scaffoldState.drawerState.close() }

                        }
                        TextBotton(
                            "Профиль",
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(start = 14.dp)
                                .padding(vertical = 4.dp)
                        )
                        {
                            navController.navigate(AppScreen.profile.name)
                            scope.launch { scaffoldState.drawerState.close() }
                        }
                        TextBotton(
                            "Настройки",
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(start = 14.dp)
                                .padding(vertical = 4.dp)
                        )
                        {
                            navController.navigate(AppScreen.setting.name)
                            scope.launch { scaffoldState.drawerState.close() }
                        }
                    },
                    topBar = {
                        topAppBar(if (currentScreen.bottomPanel) dreamUiState.dream.dream.value else currentScreen.title,
                            !currentScreen.bottomPanel,
                            onClickBack = { navController.navigateUp() },
                            onClickMenu = {
                                scope.launch{
                                    scaffoldState.drawerState.open()
                                }
                            }
                        )
                    },
                    bottomBar = {
                        if (currentScreen.bottomPanel) {
                            bottomAppBar((currentScreen.name == AppScreen.plant.name),
                                onClickPlant = {
                                         navController.navigate(AppScreen.plant.name)
                                },
                                onClickList = {
                                    if (currentScreen.name == AppScreen.plant.name) {
                                        if (!navController.popBackStack(
                                                AppScreen.list.name,
                                                false,
                                                true
                                            )
                                        ) navController.navigate(AppScreen.list.name)
                                    }
                                })
                        }
                    }
                ) { innerPadding ->

                    NavHost(
                        navController = navController,
                        startDestination = AppScreen.plant.name,
                        modifier = Modifier.padding(innerPadding)
                    ) {
                        composable(route = AppScreen.list.name) {
                            List(dreamUiState.dream.managerList, contex)
                            dreamUiState.drawerGesturesEnabled.value = true
                        }
                        composable(route = AppScreen.setting.name) {
                            Setting(contex)  { this@HostList.finish()}
                            dreamUiState.drawerGesturesEnabled.value = false
                        }
                        composable(route = AppScreen.profile.name) {
                            Profile(contex, dreamUiState.dream)
                            dreamUiState.drawerGesturesEnabled.value = false
                        }
                        composable(route = AppScreen.plant.name) {
                            Plant(
                                dreamUiState.dream.managerList.percent(),
                                dreamUiState.dream.plant,
                                contex,
                                hostModel.firstFlag,
                                { hostModel.firstFlag = false},
                                { this@HostList.finishAffinity()})
                            dreamUiState.drawerGesturesEnabled.value = true
                        }
                    }
                }
            }
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            if (currentScreen.name == AppScreen.plant.name ) {
                this@HostList.finishAndRemoveTask()
                this@HostList.finishAffinity()
                //navController.navigateUp()
                Log.i("Навигация", "выход")
            } else {
                navController.clearBackStack(AppScreen.plant.name)
                Log.i("Навигация", "На растение")
            }
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onStop() {
        super.onStop()
        val fil = fileManager("localData")
        fil.inFile(this@HostList)
    }

}
