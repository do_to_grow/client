package com.example.myapplication.main

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.example.myapplication.generalUnit.IconArrowBack
import com.example.myapplication.generalUnit.IconMenu

@Composable
fun topAppBar(
    header: String,
    isBack: Boolean,
    onClickBack: ()->Unit,
    onClickMenu: ()->Unit
){

    Row(
        horizontalArrangement = Arrangement.Start,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.background(MaterialTheme.colors.onBackground)
            .fillMaxWidth()
            .padding(7.dp, 3.dp)
    ) {
        if(isBack){
            IconButton(onClick = onClickBack) {
                IconArrowBack()
            }
        } else {
            IconButton(onClick = onClickMenu) {
                IconMenu()
            }
        }

        Spacer(modifier = Modifier.width(7.dp))
        Text(
            text = header,
            style = MaterialTheme.typography.h1,
            color = MaterialTheme.colors.background,
            textAlign = TextAlign.Center,
            modifier = Modifier.padding(start = 14.dp)
        )
    }
}