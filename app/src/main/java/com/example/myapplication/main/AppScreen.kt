package com.example.myapplication.main

import com.example.myapplication.R
import com.example.myapplication.data.localdata

enum class AppScreen(val title: String, val bottomPanel: Boolean) {
    list(title = "Список целей", true),
    plant(title = "Растение", true),
    setting(title = "Настройки", false),
    profile(title = "Профиль", false);
}
