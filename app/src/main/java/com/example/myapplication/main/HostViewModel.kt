package com.example.myapplication.main


import android.util.Log
import android.widget.Toast
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myapplication.data.localdata
import com.example.myapplication.network.DreamApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import retrofit2.HttpException
import retrofit2.http.Query
import java.io.IOException

class HostViewModel(): ViewModel() {
    private val _state = MutableStateFlow(UiHostState())
    val uiState: StateFlow<UiHostState> = _state.asStateFlow()

    //хитрая схема для загрузки растений только в случае первого захода
    private val _firstFlag: MutableState<Boolean> =  mutableStateOf(false)
//    private var firstFlagCheck: Boolean = false
//    var firstFlag: Boolean
//    get() {
//        Log.i("firstFlag", "get")
//        return if (firstFlagCheck) false else _firstFlag.value}
//    set(value) {
//        if(_firstFlag.value == true && !value) firstFlagCheck = true
//        Log.i("firstFlag", "set")
//        _firstFlag.value = value}

    var firstFlag: Boolean
        get() {
            Log.i("firstFlag", "get")
           return _firstFlag.value }
        set(value) {
            Log.i("firstFlag", "set")
            _firstFlag.value = value}


    init{
        for (stage in _state.value.dream.managerList.stages) {
            if (stage.checkState()) stage.state = false

        }

        if (localdata.actualUserDream!!.plant.stagesFlower == 0) _firstFlag.value = true

        //Запустится один раз за запуск активити, но проблема в том, что в приложении несколько активити
        if (localdata.Remender && localdata.userId != null && localdata.userId != 0) {
            loadRemenderCheck()
        }
    }

    fun loadRemenderCheck() {
        viewModelScope.launch {
            try {
                _state.value.remenderShow.value = DreamApi.retrofitService.checkRemender(localdata.userId!!)
                localdata.Remender = false
            } catch (e: IOException) {
                Log.i("Показ напоминания", "IOException")
            } catch (e: HttpException) {
                Log.i("Показ напоминания", "HttpException")
            }
        }
    }
}