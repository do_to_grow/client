package com.example.myapplication.generalUnit

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
fun textField(
    valu: String,
    text: String = "",
    enabled: Boolean = true,
    onValue: (String)->Unit = {},
    icon:  @Composable () -> Unit = {},
    modifier: Modifier = Modifier.fillMaxWidth(0.9f)
) {
    TextField(
        value = valu,
        enabled = enabled,
        textStyle = MaterialTheme.typography.body1,
        onValueChange = onValue,
        placeholder = { Text(text) },
        colors = TextFieldDefaults.textFieldColors(
            textColor = MaterialTheme.colors.surface,
            backgroundColor = MaterialTheme.colors.background,
            placeholderColor = MaterialTheme.colors.onSurface,
        ),
        trailingIcon = icon,
        modifier = modifier,
        maxLines = 2
    )
}

