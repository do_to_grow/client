package com.example.myapplication.generalUnit

import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign

@Composable
fun TextBotton(
    header: String,
    isActive: Boolean = true,
    modifier: Modifier = Modifier,
    onclick: ()->Unit
) {
    TextButton(
        onClick = onclick, enabled = isActive )
    {
        Text(
            text = header,
            style = MaterialTheme.typography.body1,
            color = if (isActive) MaterialTheme.colors.secondary else MaterialTheme.colors.onError,
            modifier = modifier,

        )
    }
}