package com.example.myapplication.generalUnit

import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.myapplication.R


@Composable
fun IconTrash(description: String, tint : Color){
    Icon(
        painter = painterResource(R.drawable.vector_trash),
        contentDescription = description,
        modifier = Modifier.height(14.3f.dp).width(13.dp),
        tint = tint
    )
}

@Composable
fun IconPlant(
    tint : Color){
    Icon(
        painter = painterResource(R.drawable.ic_plant_menu),
        contentDescription = stringResource(R.string.plant_name_view),
        modifier = Modifier.height(20.dp),
        tint = tint
    )
}

@Composable
fun IconList(
    tint : Color){
    Icon(
        painter = painterResource(R.drawable.ic_list_menu),
        contentDescription = stringResource(R.string.list_name_view),
        modifier = Modifier.height(20.dp),
        tint = tint
    )

}

@Composable
fun IconArrowDown(){
    Icon(
        painter = painterResource(R.drawable.down_arrow),
        contentDescription = stringResource(R.string.decsription_icon_arrow_down),
        modifier = Modifier.width(20.dp),
       tint = MaterialTheme.colors.secondary
    )
}

@Composable
fun IconArrowRight(){
    Icon(
        painter = painterResource(R.drawable.right_arrow),
        contentDescription = stringResource(R.string.description_icon_arrow_right),
        modifier = Modifier.height(20.dp),
        tint = MaterialTheme.colors.onBackground
    )
}

@Composable
fun IconArrowBack(){
    Icon(
        painter = painterResource(R.drawable.back_arrow),
        contentDescription = stringResource(R.string.description_back),
        modifier = Modifier.height(20.dp),
        tint = MaterialTheme.colors.background
    )
}

//true, если активен
@Composable
fun BackToArray(boolean: Boolean){
    Icon(
        painter = painterResource(R.drawable.left_arrow),
        contentDescription = "к предыдущей цели",
        modifier = Modifier.height(20.dp),
        tint = if (boolean) MaterialTheme.colors.secondary else MaterialTheme.colors.onError
    )
}

@Composable
fun ForwardToArray(boolean: Boolean){
    Icon(
        painter = painterResource(R.drawable.right_arrow),
        contentDescription = "к следующей цели",
        modifier = Modifier.height(20.dp),
        tint = if (boolean) MaterialTheme.colors.secondary else MaterialTheme.colors.onError
    )
}

@Composable
fun IconMenu(){
    Icon(
        painter = painterResource(R.drawable.menu),
        contentDescription = "Открыть меню",
        modifier = Modifier.height(25.dp),
        tint = MaterialTheme.colors.background
    )
}

@Composable
fun IconLoad(){
    Icon(
        painter = painterResource(R.drawable.loading_img),
        contentDescription = "Загрузка",
//        modifier = Modifier.height(400.dp),
        //tint = MaterialTheme.colors.b5ackground
    )
}