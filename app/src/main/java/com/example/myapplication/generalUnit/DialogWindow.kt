package com.example.myapplication.list

import androidx.compose.foundation.layout.*
import androidx.compose.material.AlertDialog
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.example.myapplication.R
import com.example.myapplication.generalUnit.filledButton


@Composable
fun DialogWindow(
    alertFlag: Boolean,
    header: String? = null,
    description: String? = null,
    onDismissRequest: ()->Unit,
    buttons: @Composable ()->Unit,
    main: @Composable ()->Unit = {}
) {
    if(alertFlag) {
        AlertDialog(
            onDismissRequest = onDismissRequest,
            title = {
                Text(
                    text = header ?: stringResource(R.string.dialog_window_header),
                    style = MaterialTheme.typography.h1,
                    color = MaterialTheme.colors.error,
                    textAlign = TextAlign.Center,
                    modifier = Modifier.fillMaxWidth()
                )
            },
            text = {
                Box(contentAlignment = Alignment.Center) {
                    if (description != null) {
                    Text(
                        text = description,
                        style = MaterialTheme.typography.body1,
                        color = MaterialTheme.colors.surface,
                        textAlign = TextAlign.Center,
                        modifier=Modifier.fillMaxWidth()
                    )} else {
                        main()
                    }
                }
            },
            buttons = {
                Row(
                    modifier = Modifier
                        .padding(bottom = 8.dp)
                        .fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceEvenly
                ) {
                    buttons()
                }
            },
            shape = MaterialTheme.shapes.large,
            backgroundColor = MaterialTheme.colors.background
        )
    }
}