package com.example.myapplication.generalUnit

import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable

@Composable
fun filledButton(
    text: String,
    enabled: Boolean = true,
    onClick: () -> Unit
) {
    Button(
        onClick = onClick,
        enabled = enabled,
        colors = ButtonDefaults.buttonColors(
            backgroundColor = MaterialTheme.colors.onBackground,
            disabledBackgroundColor = MaterialTheme.colors.background
        )
    ){

        Text(
            text = text,
            style = MaterialTheme.typography.body1,
            color = MaterialTheme.colors.background
        )
    }
}