package com.example.myapplication.plant


import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.myapplication.R
import androidx.compose.foundation.layout.*
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.text.style.TextAlign
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.myapplication.data.Flower
import com.example.myapplication.generalUnit.*
import com.example.myapplication.list.DialogWindow
import com.example.myapplication.registration.FirstStep

@Composable
fun Plant(percent: Float,
          plant: Flower,
          context: Context,
          FirstFlag: Boolean,
          firstFlagChange: ()-> Unit,
          FinishActivity: ()->Unit
) {
    val plantModel: PlantModel = viewModel()

    DialogWindow(alertFlag = plantModel.alertFlag.value, description = stringResource(R.string.plant_dialog_description),
        onDismissRequest = {
        plantModel.alertFlag.value = false
    }, buttons = {
        filledButton(text = "НЕ ГОТОВ") {
            plantModel.alertFlag.value = false
        }
        filledButton("  ГОТОВ  ") {
            plantModel.alertFlag.value = false
            plantModel.finishDream(context)
            //перейти в активити регистрации
            val nav = Intent(context, FirstStep::class.java)
            context.startActivity(nav)
            //закончить это активити
            FinishActivity()
        }
    })

    Column(

        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.background(MaterialTheme.colors.background)
            .fillMaxSize()
            .padding(28.5f.dp)
    )
    {

        if (FirstFlag) {
            IsFirstEnter(
                plantModel,
                context, firstFlagChange
            )
        } else
        {
            Text(
                text = "Выполнено: ${(percent*100).toInt()}%",
                style = MaterialTheme.typography.h2,
                color = MaterialTheme.colors.surface,
                textAlign = TextAlign.Center
            )

            LinearProgressIndicator(progress = percent)

            if ((percent*100).toInt() == 100) {
                Spacer(modifier = Modifier.height(4.dp))

                Text(
                    text = "${stringResource(R.string.finish_dream_text_1)}\n" +
                            "${stringResource(R.string.finish_dream_text_2)}\n" +
                            "${stringResource(R.string.finish_dream_text_3)}\n" +
                            stringResource(R.string.welcome_part4),
                    style = MaterialTheme.typography.h3,
                    color = MaterialTheme.colors.surface,
                    textAlign = TextAlign.Center
                )

                TextBotton(stringResource(R.string.start_new_dream)) {
                    //Уведомить пользовтеля что назад пути не будет
                    plantModel.alertFlag.value = true
                }
                Spacer(modifier = Modifier.height(28.dp))
            } else Spacer(modifier = Modifier.height(42.dp))

            Column(
                verticalArrangement = Arrangement.Bottom,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxSize()
                    .verticalScroll(rememberScrollState())
            ) {
                LoadImage(plantModel, plant, percent, context)
            }
        }
    }
}
