package com.example.myapplication.plant

import android.content.Context
import android.graphics.Bitmap
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.unit.dp
import com.example.myapplication.data.Flower
import com.example.myapplication.data.localdata

@Composable
fun LoadImage(
    plantModel: PlantModel,
    plant: Flower,
    percent: Float,
    context: Context
){
    plantModel.loadImage(context, percent, plant)

    Image(
        bitmap = Bitmap.createScaledBitmap(
            plantModel.image,
            ((plantModel.image.width) * (localdata.SettingState.plantScale.value + 0.6f)).toInt(),
            ((plantModel.image.height) * (localdata.SettingState.plantScale.value + 0.6f)).toInt(),
            true
        ).asImageBitmap(),
        contentDescription = "Описание",
        modifier = Modifier.width(400.dp)
    )
}