package com.example.myapplication.plant

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.example.myapplication.data.Flower
import com.example.myapplication.data.OldDream
import com.example.myapplication.network.Plant
import com.example.myapplication.data.localdata
import com.example.myapplication.network.LoadState
import com.example.myapplication.network.DreamApi
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException

class PlantModel(): ViewModel() {
    var image: Bitmap =
        Bitmap.createBitmap(200, 300, Bitmap.Config.ARGB_8888)
    val loadState : MutableState<Boolean> = mutableStateOf(false)
    var state: LoadState by mutableStateOf(LoadState.Loading)
        private set
    var alertFlag : MutableState<Boolean> = mutableStateOf(false)

    fun reLoad(ctx: Context){
        state = LoadState.Loading

        viewModelScope.launch {
           createFlower(
                localdata.actualUserDream?.managerList?.allStage() ?: 1, ctx
            )
        }
    }

    fun finishDream(context: Context) {

        val nameFile: String

        //придумать схему нейминга для картинок растеинй для старых мечт
        if (localdata.dreamList == null) {
            nameFile = "OldPlant0"

            //создать новый файл с картинкой растения мечты (скопировать)
            val file =
                File(context.filesDir, "Plant${localdata.actualUserDream!!.plant.stagesFlower - 1}")
            file.renameTo(File(context.filesDir, nameFile))
            val oldDream = mutableListOf(
                OldDream(
                    localdata.actualUserDream!!.dream.value,
                    localdata.actualUserDream!!.motive.value,
                    nameFile
                )
            )
            //занести новую мечту в старую
            localdata.dreamList = oldDream

        } else {
            nameFile = "OldPlant${localdata.dreamList!!.size}"

            //создать новый файл с картинкой растения мечты (скопировать)
            val file =
                File(context.filesDir, "Plant${localdata.actualUserDream!!.plant.stagesFlower - 1}")
            file.renameTo(File(context.filesDir, nameFile))

            //занести новую мечту в старую
            localdata.dreamList!!.add(
                OldDream(
                    localdata.actualUserDream!!.dream.value,
                    localdata.actualUserDream!!.motive.value,
                    nameFile
                )
            )
        }
//        Обнулить актуальную мечту
        localdata.actualUserDream = null
    }

    fun loadImage(context: Context, percent: Float, plant: Flower) {

        var number: Int = if ((percent*100).toInt() == 100) {
            plant.stagesFlower - 1
        } else ((plant.stagesFlower - 1) * percent).toInt()

        Log.i("number", number.toString())

        if (plant.completeCount >= number){
            number = plant.completeCount
        } else {
            for (i in plant.completeCount..(number - 1)) {
                File(context.filesDir, "Plant${i}").delete()
            }
            plant.completeCount = number
        }

        try {
            val fin: FileInputStream = context.openFileInput("Plant$number")
            image = BitmapFactory.decodeStream(fin)
            fin.close()

        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    suspend fun createFlower(stages: Int, ctx: Context) {
        var list: List<Plant> = listOf()//Plant("wait.png"))
        var flagFailed = false

        viewModelScope.launch {
            //получение нужных картинок из бд
            state = try {
                list = DreamApi.retrofitService.getFlower(stages)
                Log.i("FileManager", stages.toString())
                Log.i("FileManager", list.toString())
                LoadState.Success
            } catch (e: IOException) {
                LoadState.Error
            } catch (e: HttpException) {
                LoadState.Error
            }
            for (imageSrc in list) {
                Glide.with(ctx)
                    .asBitmap()
                    .load("http://BASE_URL/storage/plants/${imageSrc.src_frame}")
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: Transition<in Bitmap>?
                        ) {
                            val nameJpg = "Plant${list.indexOf(imageSrc)}"
                            try {
                                val fos: FileOutputStream =
                                    ctx.openFileOutput(nameJpg, Context.MODE_PRIVATE)
                                resource.compress(Bitmap.CompressFormat.PNG, 100, fos)
                                if(list.indexOf(imageSrc) == (list.size - 1)) loadState.value = true
                            } catch (e: IOException) {
                                e.printStackTrace()
                            }
                        }

                        override fun onLoadFailed(errorDrawable: Drawable?) {
                            flagFailed =  true
                        }
                        override fun onLoadCleared(placeholder: Drawable?) {}
                    })


            }
            if (!flagFailed) localdata.actualUserDream?.plant?.stagesFlower = list.size
        }.join()
    }

}
