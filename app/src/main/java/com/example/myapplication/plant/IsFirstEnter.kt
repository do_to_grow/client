package com.example.myapplication.plant

import android.annotation.SuppressLint
import android.content.Context
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import com.example.myapplication.R
import com.example.myapplication.data.localdata
import com.example.myapplication.generalUnit.IconLoad
import com.example.myapplication.generalUnit.TextBotton
import com.example.myapplication.network.LoadState
import kotlinx.coroutines.launch


@SuppressLint("CoroutineCreationDuringComposition")
@Composable
fun IsFirstEnter(
    hostModel: PlantModel,
    context: Context,
    changeFlag: ()->Unit
) {
    val scope = rememberCoroutineScope()
    scope.launch {
        hostModel.createFlower(
            localdata.actualUserDream?.managerList?.allStage() ?: 1, context
        )
    }


    when (hostModel.state) {
        is LoadState.Loading -> {
            Text(
                text = "${stringResource(R.string.welcome_part1)}\n" +
                        "${stringResource(R.string.welcome_part2)}\n" +
                        "${stringResource(R.string.welcome_part3)}\n" +
                        stringResource(R.string.welcome_part4),
                        style = MaterialTheme.typography.h3,
                color = MaterialTheme.colors.surface,
                textAlign = TextAlign.Center
            )
            IconLoad()
        }
        is LoadState.Success -> {
            if (!hostModel.loadState.value) IconLoad()
            else changeFlag()
        }
        is LoadState.Error -> {
            Text(
                text = stringResource(R.string.error_in_load_data),
                style = MaterialTheme.typography.h3,
                color = MaterialTheme.colors.surface,
                textAlign = TextAlign.Center
            )

            TextBotton("Повторить") {
                hostModel.reLoad(context)
            }
        }
    }
}

