package com.example.myapplication.registration

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.example.myapplication.ui.theme.MyApplicationTheme
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.myapplication.generalUnit.filledButton
import com.example.myapplication.generalUnit.textField


class FirstStep : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApplicationTheme {
                Column(
                    verticalArrangement = Arrangement.Top,
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier.background(MaterialTheme.colors.background)
                        .fillMaxSize()
                        .padding(horizontal = 28.5f.dp, vertical = 57.dp)
                        .verticalScroll(rememberScrollState())
                )
                {
                    val RegModel: RegModel = viewModel()
                    val dreamUiState by RegModel.uiState.collectAsState()

                    Text(
                        text = "Какая у вас мечта",
                        style = MaterialTheme.typography.h2,
                        color = MaterialTheme.colors.surface
                    )
                    Spacer(modifier = Modifier.height(112.dp))

                    textField(
                        valu = dreamUiState.dream.value, text = "Ваша мечта",
                        onValue = {newText -> if (newText.length < 21)
                            dreamUiState.dream.value = newText
                        },
                        icon = {}
                    )

                    Spacer(modifier = Modifier.height(14.dp))

                    textField(
                        dreamUiState.motive.value, "Ваша мотивация",
                        onValue = {newText -> dreamUiState.motive.value = newText},
                        icon = {}
                    )

                    Spacer(modifier = Modifier.height(72.dp))

                    filledButton("ПРОДОЛЖИТЬ"){
                        if(dreamUiState.dream.value.length > 0 && dreamUiState.motive.value.length > 0) {
                            val navToStep = Intent(this@FirstStep, SecondStep::class.java)
                            RegModel.firstStep()
                            this@FirstStep.startActivity(navToStep)
                        } else Toast.makeText(this@FirstStep, "Поля мечты или мотивации не могут быть пустыми", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }
}


