package com.example.myapplication.registration

import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.myapplication.generalUnit.IconTrash
import com.example.myapplication.generalUnit.filledButton
import com.example.myapplication.generalUnit.textField
import com.example.myapplication.data.Stage

//внешний вид списка этапов в режиме редактирования
@Composable
fun ElStage(stages: MutableList<Stage>,
            ctx:  android.content.Context,

) {
    Column(
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .background(MaterialTheme.colors.background)
            .fillMaxSize()
    ) {
        for (stage in stages) {
            stage.name.let {
                val trailingIconView = @Composable {
                    IconButton(
                        onClick = {
                            if (stages.size > 1) {
                                stages.remove(stage)

                            } else Toast.makeText(
                                ctx,
                                "В мечте должен быть минимум 1 этап",
                                Toast.LENGTH_SHORT
                            ).show()
                        },
                    ) {
                        IconTrash("Удалить цель", if (stages.size > 1) MaterialTheme.colors.error else MaterialTheme.colors.onSurface)
                    }
                }

                textField(
                    it.value, "Ваш этап",
                    onValue = { newText ->
                        it.value = newText
                       },
                    icon = trailingIconView
                )

                ElGoals(stage.goals, ctx)
            }
            Spacer(modifier = Modifier.height(7.dp))
        }

        Spacer(modifier = Modifier.height(14.dp))

        filledButton("ДОБАВИТЬ ЭТАП"){
                stages.add(Stage(mutableStateOf("")))
        }
    }
}