package com.example.myapplication.registration

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.myapplication.generalUnit.filledButton
import com.example.myapplication.main.HostList
import com.example.myapplication.ui.theme.MyApplicationTheme

class SecondStep : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApplicationTheme {
                val ctx = LocalContext.current
                // A surface container using the 'background' color from the theme
                Column(
                    verticalArrangement = Arrangement.Top,
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier
                        .background(MaterialTheme.colors.background)
                        .fillMaxSize()
                        .verticalScroll(rememberScrollState(), reverseScrolling = true)
                        .padding(horizontal = 28.5f.dp, vertical = 57.dp)
                )
                {
                    Text(
                        text = "Что необходимо сделать для осуществления вашей мечты?",
                        style = MaterialTheme.typography.h2,
                        color = MaterialTheme.colors.surface,
                        textAlign = TextAlign.Center
                    )

                    val RegModel: RegModel = viewModel()
                    val dreamUiState by RegModel.uiState.collectAsState()

                    Spacer(modifier = Modifier.height(14.dp))

                    Box(
                        modifier = Modifier
                            .fillMaxWidth()
                            .fillMaxHeight(0.85f)
                    )
                    {
                        Spacer(modifier = Modifier.height(46.dp))
                        ElStage(dreamUiState.managerList.stages, LocalContext.current)
                    }

                    Spacer(modifier = Modifier.height(14.dp))

                    filledButton(text = "ПРОДОЛЖИТЬ") {
                        if (!dreamUiState.managerList.checkName())
                            Toast.makeText(
                                this@SecondStep,
                                "Поля этапов и целей не могут быть пустыми",
                                Toast.LENGTH_SHORT
                            ).show() else {
                            RegModel.secondStep(ctx)//сохранение данных
                            val navToStep = Intent(this@SecondStep, HostList::class.java)
                            navToStep.putExtra("first", 1)
                            this@SecondStep.startActivity(navToStep)
                        }
                    }
                }
            }
        }
    }
}

