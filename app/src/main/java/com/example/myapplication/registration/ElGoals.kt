package com.example.myapplication.registration

import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.myapplication.generalUnit.IconTrash
import com.example.myapplication.generalUnit.TextBotton
import com.example.myapplication.generalUnit.textField
import com.example.myapplication.data.Goal

//внешний вид списка целей внутри этапа в режиме редактировании
@Composable
fun ElGoals(goals: MutableList<Goal>,
            ctx:  android.content.Context,
            ){
    Column(
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.End,
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 28.5f.dp)
    ) {
        for (goal in goals) {
            goal.name.let {
                val trailingIconView = @Composable {
                    IconButton(
                        onClick = {
                            if (goals.size > 1) {
                                goals.remove(goal)
                            } else Toast.makeText(
                                ctx,
                                "В этапе должна быть минимум 1 цель",
                                Toast.LENGTH_SHORT
                            ).show()
                        },
                    ) {
                        IconTrash("Удалить цель", if (goals.size > 1) MaterialTheme.colors.error else MaterialTheme.colors.onSurface)
                    }
                }
                textField(
                    it.value, "Ваша цель в этапе",
                    onValue = { newText ->
                        it.value = newText
                        },
                    icon = trailingIconView
                )
            }
            Spacer(modifier = Modifier.height(4.dp))
        }

       Row(
            horizontalArrangement = Arrangement.End,
            modifier = Modifier.fillMaxWidth(0.9f)
        ) {
            TextBotton("Добавить цель") {
                goals.add(Goal(mutableStateOf("")))
            }
       }
    }
}