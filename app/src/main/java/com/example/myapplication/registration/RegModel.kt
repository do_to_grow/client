package com.example.myapplication.registration

import android.content.Context
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myapplication.data.*
import com.example.myapplication.entrance.fileManager
import com.example.myapplication.network.DreamApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException


class RegModel() : ViewModel() {

    private val _dream = MutableStateFlow(UserDream())
    val uiState: StateFlow<UserDream> = _dream.asStateFlow()
    init {
        if (localdata.actualUserDream != null)
            _dream.value = localdata.actualUserDream!!
        else {
            _dream.value.dream.value = ""
            _dream.value.motive.value = ""
            _dream.value.managerList.stages.clear()
            _dream.value.managerList.stages.add(Stage())
        }
    }

    fun firstStep() {
        localdata.actualUserDream = _dream.value
    }

    fun secondStep(ctx: Context) {
        localdata.actualUserDream = _dream.value
        if (localdata.userId == null) createUser()

        val z = fileManager("localData")
        z.inFile(ctx)
    }

    private fun createUser() {
        viewModelScope.launch {
            localdata.userId = try {
                DreamApi.retrofitService.getUser()
            } catch (e: IOException) {
                0
            } catch (e: HttpException) {
                0
            }
        }

        Log.i("UserID", localdata.userId.toString())
    }

}