package com.example.myapplication.viewStage

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.myapplication.generalUnit.*
import com.example.myapplication.ui.theme.MyApplicationTheme
import androidx.compose.material.TextField
import com.example.myapplication.R
import com.example.myapplication.main.topAppBar

class ViewOneGoal : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApplicationTheme {
                val context = LocalContext.current
                val intent = (context as ViewOneGoal).intent
                val stageIndex = intent.getIntExtra("StageIndex", -1)
                val goalIndex = intent.getIntExtra("GoalIndex", 0)

                if (stageIndex > -1) {
                    Column(
                        verticalArrangement = Arrangement.Top,
                        modifier = Modifier.fillMaxSize())
                    {

                        val model: viewOneGoalModel = viewModel()
                        model.start(stageIndex, goalIndex)
                        val uiState by model.uiState.collectAsState()

                        topAppBar(uiState.stage.name.value, true, onClickBack = { this@ViewOneGoal.finish() }, {})

                        Column(
                            modifier = Modifier.fillMaxWidth().verticalScroll(rememberScrollState())
                                .padding( horizontal = 28.5f.dp)
                                .padding(top = 7.dp)
                        ) {
                            HeaderContent(uiState)
                        }
                    }

                } else Box(
                    contentAlignment = Alignment.Center,
                    modifier = Modifier.fillMaxSize()
                ){Text(getString(R.string.error_in_load_data))}
            }
        }
    }
}

@Composable
fun HeaderContent(state: viewStageState) {
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ){
        IconButton(onClick = {state.indexGoal.value--}, enabled = state.backState(),  modifier = Modifier.fillMaxWidth().weight(0.05f)) {
            BackToArray(state.backState())
        }
        //Spacer(modifier = Modifier.width(16.dp))
        Row(
            horizontalArrangement =Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.fillMaxWidth().weight(0.9f)
        ) {

            Checkbox(
                checked = state.actualGoal().state,
                onCheckedChange = { value -> state.actualGoal().state = value },
                modifier = Modifier.height(20.dp),
                //enabled = !goal.state,
                colors = CheckboxDefaults.colors(
                    checkedColor = MaterialTheme.colors.secondary,
                    uncheckedColor = MaterialTheme.colors.secondary,
                    checkmarkColor = MaterialTheme.colors.primary,
                    disabledColor = MaterialTheme.colors.onError
                )
            )
            Text(
                text = state.actualGoal().name.value,
                style = MaterialTheme.typography.h2,
                color = if (state.actualGoal().state) MaterialTheme.colors.onSurface else MaterialTheme.colors.surface
            )
        }
        IconButton(onClick = {state.indexGoal.value++}, enabled = state.forwardState(), modifier = Modifier.fillMaxWidth().weight(0.05f)) {
            ForwardToArray(state.forwardState())
        }
    }
    Text(
        text = "Заметки и описание",
        style = MaterialTheme.typography.body2,
        color = MaterialTheme.colors.onSurface,
        modifier = Modifier.padding(start = 14.dp, top = 7.dp)
    )
    TextField(
        value = state.actualGoal().description,
        textStyle = MaterialTheme.typography.body1,
        onValueChange = { value ->
            state.actualGoal().description = value
        },
        placeholder = {Text("Описание цели")},
        colors = TextFieldDefaults.textFieldColors(
            textColor = MaterialTheme.colors.surface,
            backgroundColor = MaterialTheme.colors.background,
            placeholderColor = MaterialTheme.colors.onSurface,
            cursorColor = MaterialTheme.colors.background,
        ), modifier = Modifier.padding(horizontal = 7.dp).fillMaxWidth().padding(bottom = 14.dp)
    )
}
