package com.example.myapplication.viewStage

import androidx.lifecycle.ViewModel
import com.example.myapplication.data.localdata
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class viewOneGoalModel() : ViewModel()  {
    private val _stageState = MutableStateFlow(viewStageState())
    val uiState: StateFlow<viewStageState> = _stageState.asStateFlow()

    init {
    }

    fun start(indexStage: Int, indexGoal: Int): Boolean{
        if (localdata.actualUserDream == null ) return false
        uiState.value.stage = localdata.actualUserDream!!.managerList.stages[indexStage]
        uiState.value.indexStage = indexStage
        uiState.value.indexGoal.value = indexGoal
        return true
    }
}