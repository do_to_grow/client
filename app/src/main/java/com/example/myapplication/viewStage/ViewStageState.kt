package com.example.myapplication.viewStage

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import com.example.myapplication.data.Goal
import com.example.myapplication.data.Stage


data class viewStageState(

    var stage: Stage = Stage(),
    var indexStage: Int = -1,
    var indexGoal: MutableState<Int> = mutableStateOf(0),
){
    fun backState(): Boolean{
        if (indexGoal.value - 1 < 0)
            return false
        return true
    }

    fun forwardState(): Boolean{
        if (indexGoal.value + 1 >= stage.goals.size) {
            return false
        }
        return true
    }

    fun actualGoal(): Goal {
        return stage.goals[indexGoal.value]
    }
}