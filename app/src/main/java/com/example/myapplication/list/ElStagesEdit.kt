package com.example.myapplication.list

import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.myapplication.data.Stage
import com.example.myapplication.generalUnit.*
import com.example.myapplication.registration.ElGoals


@Composable
fun ElStagesEdit(stages: MutableList<Stage>,
                 ctx:  android.content.Context,
) {
    Column(
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .background(MaterialTheme.colors.background)
            .fillMaxSize()
    ) {
        for (stage in stages) {

            stage.name.let {
                val trailingIconView = @Composable {
                    IconButton(
                        onClick = {
                            if (stages.size > 1) {
                                stages.remove(stage)

                            } else Toast.makeText(
                                ctx,
                                "В мечте должен быть минимум 1 этап",
                                Toast.LENGTH_SHORT
                            ).show()
                        },
                    ) {
                        IconTrash(
                            "Удалить цель",
                            if (stages.size > 1) MaterialTheme.colors.error else MaterialTheme.colors.onSurface
                        )
                    }
                }
                Row(
                    horizontalArrangement = Arrangement.Center,
                    verticalAlignment = Alignment.CenterVertically
                ) {

                    textField(
                        it.value, "Ваш этап",
                        onValue = { newText ->
                            it.value = newText
                        },
                        icon = trailingIconView
                    )

                    IconButton(
                        modifier = Modifier.fillMaxWidth(),
                        onClick = {
                            if (stage.state)
                                stage.state = false
                            else stage.state = true
                        },
                    ) {
                        if (stage.state) IconArrowDown() else IconArrowRight()
                    }

                }

            }
            if (stage.state) {
                ElGoals(stage.goals, ctx)
            }
            Spacer(modifier = Modifier.height(7.dp))
        }
        Spacer(modifier = Modifier.height(14.dp))

        filledButton("ДОБАВИТЬ ЭТАП") {
            stages.add(Stage(mutableStateOf("")))
        }
    }
}