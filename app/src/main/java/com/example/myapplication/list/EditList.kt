package com.example.myapplication.list

import android.widget.Toast
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.example.myapplication.generalUnit.filledButton


@Composable
fun EditList(
    listModel: listEditModel,
    contex: android.content.Context,
    listState: ListState
){
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight(0.85f)
    )
    {
        ElStagesEdit(listState.list.stages, contex)
    }

    filledButton("СОХРАНИТЬ"){
        if (!listState.list.checkName()){
            Toast.makeText(
                contex,
                "Поля этапов и целей не могут быть пустыми",
                Toast.LENGTH_SHORT
            ).show() } else {
            listModel.save()
        }
    }
}