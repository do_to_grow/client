package com.example.myapplication.main


import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.myapplication.R
import com.example.myapplication.data.ListManager
import com.example.myapplication.generalUnit.filledButton
import com.example.myapplication.list.*


@Composable
fun List(
    list:  ListManager,
    contex: android.content.Context
) {
    val listModel: listEditModel = viewModel()
    val uiState by listModel.uiState.collectAsState()

        DialogWindow( alertFlag = uiState.isAlert.value, description= "Это действие может повлечь за собой отмену изменений",
            onDismissRequest =
            {
                uiState.isAlert.value = false
                uiState.isSave.value = true
                uiState.isView.value = false
            },
            buttons =
            {
                filledButton(text = "НЕ ГОТОВ") {
                    uiState.isSave.value = true
                    uiState.isView.value = false
                    uiState.isAlert.value = false
                }
                filledButton("  ГОТОВ  ") {
                    uiState.isSave.value = false
                    uiState.isAlert.value = false
                }
            })


    Column(
        verticalArrangement = Arrangement.Top,
        modifier = Modifier.fillMaxSize()
    ){
        if (list.percent() != 1.0f) {
            Row(
                horizontalArrangement = Arrangement.SpaceEvenly,
                modifier = Modifier.background(MaterialTheme.colors.primary)
                    .padding(bottom = 0.5f.dp)
                    .background(MaterialTheme.colors.background)
                    .fillMaxWidth()
                    .padding(14.dp, 0.dp)

            ) {
                TextButtonMenu(
                    stringResource(R.string.view_name_view),
                    !uiState.isView.value,
                    modifier = Modifier.weight(1f)
                ) { uiState.isView.value = true }
                TextButtonMenu(
                    stringResource(R.string.edit_name_view),
                    uiState.isView.value,
                    modifier = Modifier.weight(1f)
                ) { uiState.isView.value = false }
            }
        }
        Column(
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .fillMaxSize()
                .verticalScroll(rememberScrollState(), reverseScrolling = true)
                .padding(horizontal = 28.5f.dp, vertical = 14.dp)
        ){
            if(uiState.isView.value) {
               if (uiState.isSave.value) uiState.isAlert.value = true
                if (!uiState.isSave.value) ViewList(list, contex)
            } else {
                listModel.copy()
                //уведомить согласен ли он отменить изменения (проверка были ли изменения
                EditList(listModel, contex, uiState)
            }
        }
    }
}
