package com.example.myapplication.list

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.selection.selectable
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp


@Composable
fun TextButtonMenu(
    header: String,
    isActive: Boolean,
    modifier: Modifier,
    onclick: ()->Unit
) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier.selectable(
            selected = !isActive,
            enabled = isActive,
            onClick = onclick)
            .height(42.dp)

    ){
        Text(
            text = header,
            style = MaterialTheme.typography.body1,
            color = if (!isActive) MaterialTheme.colors.secondary else MaterialTheme.colors.onError,
        )
    }

}
