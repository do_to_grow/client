package com.example.myapplication.list

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import com.example.myapplication.data.ListManager

data class ListState(
    //буферный список целей для редактирования без последствий
    val list: ListManager,
    //true если на странице просмотра
    val isView : MutableState<Boolean> = mutableStateOf(true),
    //true если список надо сохранить
    val isSave: MutableState<Boolean> =  mutableStateOf(false),
    //true если надо вызвать alert
    val isAlert: MutableState<Boolean> =  mutableStateOf(false),
    //true если на скрине растения
    val isPlant : MutableState<Boolean> = mutableStateOf(true),
)
