package com.example.myapplication.list

import androidx.lifecycle.ViewModel
import com.example.myapplication.data.ListManager
import com.example.myapplication.data.localdata
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow


class listEditModel(): ViewModel() {
    private val _state = MutableStateFlow(ListState(ListManager()))
    val uiState: StateFlow<ListState> = _state.asStateFlow()

    init{


    }

    fun copy(){
        if(_state.value.isSave.value == false) {
            _state.value.list.stages = localdata.actualUserDream!!.managerList.copy()
            _state.value.isSave.value = true
        }
    }

    fun save(){
        localdata.actualUserDream!!.managerList.stages = _state.value.list.stages
        _state.value.isSave.value = false
        _state.value.isView.value = true
    }

//    fun load(list: ListManager){
//        viewModelScope.launch {
//            loadState = try {
//                if(uiState.value.isSave.value){
//                    uiState.value.list.stages = list.copy()
//                    uiState.value.isSave.value = true
//                }
//                LoadState.Success
//            } catch (e: IOException) {
//                LoadState.Error
//            }
//        }
//    }

}