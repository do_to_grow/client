package com.example.myapplication.list

import android.content.Intent
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.selection.selectable
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.myapplication.data.ListManager
import com.example.myapplication.generalUnit.IconArrowDown
import com.example.myapplication.generalUnit.IconArrowRight
import com.example.myapplication.viewStage.ViewOneGoal


@Composable
fun ViewList(list: ListManager, contex: android.content.Context) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.Start
    ) {


        for (stage in list.stages) {
            Row(horizontalArrangement = Arrangement.Start,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Checkbox(
                    checked = stage.checkState(),
                    onCheckedChange = { stage.setState() },
                    enabled = !stage.checkState(),
                    colors = CheckboxDefaults.colors(
                        checkedColor = MaterialTheme.colors.secondary,
                        uncheckedColor = MaterialTheme.colors.secondary,
                        checkmarkColor = MaterialTheme.colors.primary,
                        disabledColor = MaterialTheme.colors.onError
                    )
                )
                Spacer(modifier = Modifier.width(7.dp))
                Text(
                    text = stage.name.value,
                    style = MaterialTheme.typography.body1,
                    color = if (stage.checkState()) MaterialTheme.colors.onSurface else MaterialTheme.colors.surface,
                    modifier = Modifier.fillMaxHeight().fillMaxWidth(0.8f).selectable(selected = false,
                        onClick = {
                            val nav = Intent(contex, ViewOneGoal::class.java)
                            nav.putExtra("StageIndex", list.stages.indexOf(stage))
                            contex.startActivity(nav)
                        })
                )

                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.End,
                    verticalAlignment = Alignment.CenterVertically
                ){
                    IconButton(
                        onClick = {
                            if (stage.state)
                                stage.state = false
                            else stage.state = true
                        },
                    ) {
                        if (stage.state) IconArrowDown() else  IconArrowRight()
                    }
                }
            }

            if(stage.state) {
                Column(
                    modifier = Modifier.padding(start = 22.dp)
                ) {
                    for (goal in stage.goals) {
                        Row(
                            horizontalArrangement = Arrangement.Start,
                            verticalAlignment = Alignment.CenterVertically
                        ) {

                            Checkbox(
                                checked = goal.state,
                                onCheckedChange = { value -> goal.state = value },
                                //enabled = !goal.state,
                                colors = CheckboxDefaults.colors(
                                    checkedColor = MaterialTheme.colors.secondary,
                                    uncheckedColor = MaterialTheme.colors.secondary,
                                    checkmarkColor = MaterialTheme.colors.primary,
                                    disabledColor = MaterialTheme.colors.onError
                                )
                            )
                            Row(
                                modifier = Modifier.fillMaxSize(1f)
                                    .padding(7.dp)
                                    .selectable(
                                        selected = false,
                                        onClick = {
                                            val nav = Intent(contex, ViewOneGoal::class.java)
                                            nav.putExtra("StageIndex", list.stages.indexOf(stage))
                                            nav.putExtra("GoalIndex", stage.goals.indexOf(goal))
                                            contex.startActivity(nav)
                                        }
                                    )
                            ){
                                Text(
                                    text = goal.name.value,
                                    style = MaterialTheme.typography.body1,
                                    color = if (goal.state) MaterialTheme.colors.onSurface else MaterialTheme.colors.surface
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}