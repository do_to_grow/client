package com.example.myapplication.entrance

import android.content.Context
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myapplication.network.LoadState
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.io.IOException

class dataLoad() : ViewModel() {
    var state: LoadState by mutableStateOf(LoadState.Loading)
        private set

    fun load(ctx: Context){
        val na = fileManager("localData")

        viewModelScope.launch {
            state = try {
                na.inObject(ctx)
                delay(500L)
                LoadState.Success
            } catch (e: IOException) {
                LoadState.Error
            }
        }
    }
}