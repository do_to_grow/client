package com.example.myapplication.entrance

import android.content.Context
import android.util.Log
import com.example.myapplication.data.JsonLocalData
import com.example.myapplication.data.UserDream
import com.example.myapplication.data.localdata
import com.example.myapplication.setting.data.SettingState
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException

class fileManager(val FileName: String) {

    fun inFile(ctx: Context){
        //сохранение локальной даты в формат json локальной даты

            val local = JsonLocalData(
                localdata.actualUserDream?.toJsonDream(),
                localdata.userId,
                localdata.dreamList,
                localdata.SettingState.toJson()
            )

            //конвертирование json даты в строку
            val string = Json.encodeToString(local)

            //сохранение строки в файл
            try {
                val fos: FileOutputStream =
                    ctx.openFileOutput(FileName, Context.MODE_PRIVATE)
                fos.bufferedWriter(Charsets.UTF_8).use { writer ->
                    writer.write(string)
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }

        Log.i("FileManager", "SavedSuccessuly")

    }

    fun inObject(ctx: Context) {

        var string: String = ""

        try {
            val fin: FileInputStream = ctx.openFileInput(FileName)
            string = fin.bufferedReader(Charsets.UTF_8).use { it.readText() }
            fin.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        Log.i("FileManager", "OpenSuccessuly")

        //конвертирование строки в json объект
        if (string.length > 10) {
            val obj = Json.decodeFromString<JsonLocalData>(string)

            //Json объект в объект local data
            if (obj.actualUserDream != null) localdata.actualUserDream =
                UserDream(obj.actualUserDream!!)
            if (obj.userId != null) localdata.userId = obj.userId
            if (obj.dreamList != null) localdata.dreamList = obj.dreamList
            localdata.SettingState = SettingState(obj.SettingState)
            Log.i("FileManager", "Конвертирование")
        }

    }
}