package com.example.myapplication.data

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json


@Serializable
data class JsonList(
    @SerialName("stages")
    var stages: ArrayList<JsonStage>
)

class ListManager () {

    private var _stages : MutableList<Stage> = mutableStateListOf<Stage>(Stage(mutableStateOf("")))
    var stages: MutableList<Stage>
    set(value){
        _stages = value
    }
    get() = _stages

    fun checkName(): Boolean{
        for (stage in _stages){
            if (!stage.checkName())
                return false
        }
        return true
    }

    fun copy(): MutableList<Stage>{
        val stg:MutableList<Stage> = mutableStateListOf<Stage>()

        for(stage in _stages){
            stg.add(stage.copy())
        }
        return stg
    }

    fun toJson():String{
        val stages = ArrayList<JsonStage>()
        for(i in _stages)
        {
            stages.add(JsonStage(i.name.value, i.toArrayList(i.goals)))
        }
        return Json.encodeToString(JsonList(stages))
    }

    fun toObj(string: String){
        val list = Json.decodeFromString<JsonList>(string)
        _stages.clear()
        for(stg in list.stages)
        {
            stages.add(Stage(stg))
        }
    }

    constructor(list: JsonList) : this(){
        _stages.clear()
        for(stage in list.stages)
        {
            stages.add(Stage(stage))
        }
    }

    fun allStage(): Int {
        var sum = 0;
        for (stage in _stages) {
            sum += stage.goals.size
        }

        return sum
    }

    private fun countStage(): Int {
        var sum = 0;
        for (stage in _stages) {
            sum += stage.countState()
        }
        return sum
    }

   fun percent(): Float {
       val fl = (countStage() / (allStage().toFloat()))
       return fl
   }

}

