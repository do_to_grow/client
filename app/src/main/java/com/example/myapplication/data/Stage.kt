package com.example.myapplication.data

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

@Serializable
data class JsonStage(
    @SerialName("name")
    val name:  String,
    @SerialName("goals")
    var goals: ArrayList<JsonGoal>
)

class Stage (var name:  MutableState<String> = mutableStateOf("")) {

    private var _goals: MutableList<Goal> = mutableStateListOf<Goal>(Goal(mutableStateOf("")))
    var goals: MutableList<Goal>
    set(value) {
        _goals = value
    }
    get() = _goals


    //сворачивание группы целей в этапе. true если не свернуты
    private var _state: MutableState<Boolean> = mutableStateOf(true)
    var state: Boolean
        set(value){_state.value = value}
        get() = _state.value




    fun copy(): Stage{
        val stage = Stage()
        stage.name.value = name.value
        stage.goals = copyGoals(_goals)
        stage.state = false
        return stage
    }

    fun copyGoals(gls: MutableList<Goal>): MutableList<Goal> {
        val goals: MutableList<Goal> = mutableStateListOf<Goal>()
        for(std in gls){
            goals.add(std.copy())
        }
        return goals
    }

     fun checkName(): Boolean{
        if (name.value.length < 1) return false
        for (goal in _goals ){
            if (!goal.checkName())
                return false
        }
        return true
    }

    fun checkState(): Boolean{
        for (goal in _goals ){
            if (!goal.state)
                return false
        }
        return true
    }

    fun countState(): Int{
        var sum = 0
        for (goal in _goals ){
            if (goal.state) sum++
        }
        return sum
    }

    fun setState(){
        for (goal in _goals ){
           goal.state = true
        }
    }

    fun toJson():String{
        return Json.encodeToString(JsonStage(name.value, toArrayList(_goals)))
    }

    fun toArrayList(listGoals: MutableList<Goal>): ArrayList<JsonGoal>{
        val gls = ArrayList<JsonGoal>()
        for(i in listGoals.indices)
        {
            gls.add(JsonGoal(listGoals[i].name.value, listGoals[i].state, (listGoals[i].description)))
        }
        return gls
    }


    constructor(string: String) : this() {

        val stage = Json.decodeFromString<JsonStage>(string)

        name.value = stage.name
        _goals.clear()
        for(stg in stage.goals)
        {
            _goals.add(Goal(stg))
        }
    }

    constructor(stg: JsonStage) : this(){
        name.value = stg.name
        _goals.clear()
        for(stage in stg.goals)
        {
            _goals.add(Goal(stage))
        }
    }
}

