package com.example.myapplication.data

import com.example.myapplication.setting.data.JsonSettingState
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class JsonLocalData(
    @SerialName("actualUserDream")
    var actualUserDream: JsonDream?,
    @SerialName("userId")
    val userId: Int?,
    @SerialName("dreamList")
    val dreamList: MutableList<OldDream>?,
    @SerialName("setting")
    val SettingState: JsonSettingState
)
