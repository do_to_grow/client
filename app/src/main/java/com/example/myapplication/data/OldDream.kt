package com.example.myapplication.data

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class OldDream(
    @SerialName("dream")
    val dream: String,
    @SerialName("motive")
    val motive: String,
    @SerialName("picture")
    val picture: String = "/"
)
