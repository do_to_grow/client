package com.example.myapplication.data

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json


@Serializable
data class JsonDream(
    @SerialName("dream")
    val dream:  String,
    @SerialName("motive")
    val motive:  String,
    @SerialName("plant")
    val plant: Flower,
    @SerialName("stagesList")
    var manager: JsonList
)

data class UserDream(val dream: MutableState<String> = mutableStateOf(""), val motive: MutableState<String> = mutableStateOf("")) {

    private var _manager = ListManager()
val managerList: ListManager
    get(){
        return _manager
    }
    var plant: Flower = Flower()
    fun toJsonDream(): JsonDream{
        return JsonDream(dream.value, motive.value, plant, toJsonList(_manager))
    }

    fun toJson():String{
       return Json.encodeToString(JsonDream(dream.value, motive.value,  plant, toJsonList(_manager)))
    }

    fun toObj(string: String){
        val jsonDream = Json.decodeFromString<JsonDream>(string)

        dream.value = jsonDream.dream
        motive.value = jsonDream.motive
        _manager = ListManager(jsonDream.manager)
        plant = jsonDream.plant
    }

    constructor(jsonDream: JsonDream) : this(){
        dream.value = jsonDream.dream
        motive.value = jsonDream.motive
        _manager = ListManager(jsonDream.manager)
        plant = jsonDream.plant
    }

    private fun toJsonList(list: ListManager): JsonList{
        val listStages = list.stages
        val stg = ArrayList<JsonStage>()
        for(i in listStages)
        {
            stg.add(JsonStage(i.name.value, i.toArrayList(i.goals)))
        }
        return JsonList(stg)
    }

}

