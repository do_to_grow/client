package com.example.myapplication.data

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Flower(

@SerialName("stagesCount")
var stagesFlower: Int = 0,

@SerialName("completeCount")
var completeCount: Int = 0

)

