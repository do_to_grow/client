package com.example.myapplication.data

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

@Serializable
data class JsonGoal(
    @SerialName("name")
    val name:  String,
    @SerialName("state")
    var state: Boolean = false,
    @SerialName("description")
    var description: String = ""
)

//Цель
data class Goal (var name:  MutableState<String> = mutableStateOf("")) {

    private var _state: MutableState<Boolean> = mutableStateOf(false)
    var state: Boolean
        set(value){ if (value) _state.value = value}
        get() = _state.value

    private var _description: MutableState<String> = mutableStateOf("")
    var description: String
        set(value) {
            _description.value = value

        }
        get(){ return _description.value }

    fun copy(): Goal{
        val goal = Goal()
        goal.name.value = name.value
        goal.state = state
        goal.description = description
        return goal
    }

    fun checkName(): Boolean{
        return name.value.length > 0
    }

    fun toJson(): String{
        return Json.encodeToString(JsonGoal(name.value, state, description))
    }

    fun toObj(string: String): Goal{
        val goal = Json.decodeFromString<JsonGoal>(string)
        return Goal(goal)
    }

    constructor(goal: JsonGoal) : this(){
        this.name.value = goal.name
        _state.value = goal.state
        _description.value = goal.description
    }
}
