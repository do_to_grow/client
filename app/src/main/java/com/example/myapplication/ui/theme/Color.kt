package com.example.myapplication.ui.theme

import androidx.compose.ui.graphics.Color

val BlackMainBlue = Color(0xFF1C658E)
val MediumMainBlue = Color(0xFF2C7FAE)
val MenuBlue = Color(0xFF4A93BC)
val MediumLightBlue = Color(0xFF3A88B4)
val LightMainBlue= Color(0xFF48A8DE)
val ErrorColorBlue = Color(0xFFAC4242)

val BlackMainGreen = Color(0xFF05460B)
val MediumMainGreen = Color(0xFF487E3B)
val MenuGreen = Color(0xFF379B4D)
val MediumLightGreen = Color(0xFF408B34)
val LightMainGreen= Color(0xFF60B45D)
val ErrorColorGreen = Color(0xFFE95F5F)

val BlackMainViolet = Color(0xFF6B4FA6)
val MediumMainViolet = Color(0xFF8872B5)
val MenuViolet = Color(0xFF9880CB)
val MediumLightViolet = Color(0xFFA790D7)
val LightMainViolet = Color(0xFFBAA3E9)
val BackGround = Color(0xFFFAFAFA)

val Text = Color(0x9E000000)
val Signature = Color(0xFF8F8F8F)
val ErrorColorViolet = Color(0xFFC669A6)

val DeactivateBottomMenu: Color = Color(0xFFD3C9E7)
val ActiveBottomMenu: Color = Color(0xFFFAFAFA)
