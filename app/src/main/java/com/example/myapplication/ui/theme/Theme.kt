package com.example.myapplication.ui.theme

import android.annotation.SuppressLint
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.example.myapplication.R
import com.example.myapplication.data.localdata


private val VioletPalette = lightColors(
    primary = BlackMainViolet,
    primaryVariant = MenuViolet,
    secondaryVariant = BlackMainViolet,
    background = BackGround,
    secondary= MediumMainViolet,
    onBackground = MediumLightViolet,
    surface = Text,
    onSurface = Signature,
    error = ErrorColorViolet,
    onError = LightMainViolet,
)

private val GreenPalette = lightColors(
    primary = BlackMainGreen,
    primaryVariant = MenuGreen,
    secondaryVariant = BlackMainGreen,
    background = BackGround,
    secondary= MediumMainGreen,
    onBackground = MediumLightGreen,
    surface = Text,
    onSurface = Signature,
    error = ErrorColorGreen,
    onError = LightMainGreen,
)

private val BluePalette = lightColors(
    primary = BlackMainBlue,
    primaryVariant = MenuBlue,
    secondaryVariant = BlackMainBlue,
    background = BackGround,
    secondary= MediumMainBlue,
    onBackground = MediumLightBlue,
    surface = Text,
    onSurface = Signature,
    error = ErrorColorBlue,
    onError = LightMainBlue,
)

@Composable
fun MyApplicationTheme(
    content: @Composable () -> Unit
) {
    MaterialTheme(
        colors = if (localdata.SettingState.theme.value == stringResource(R.string.VioletColorName)) VioletPalette
                else if (localdata.SettingState.theme.value == stringResource(R.string.BlueColorName)) BluePalette
                else if (localdata.SettingState.theme.value == stringResource(R.string.GrrenColorName)) GreenPalette
                else VioletPalette,
        typography = WayTypography,
        shapes = Shapes,
        content = content
    )
}
