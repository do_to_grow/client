package com.example.myapplication.profile


import android.content.Context
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.myapplication.data.localdata
import androidx.compose.ui.text.style.TextAlign
import com.example.myapplication.data.UserDream
import kotlin.random.Random

@Composable
fun Profile(
    context: Context,
    dream: UserDream
) {
    Column(
        verticalArrangement = Arrangement.Top,
        //horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState(), reverseScrolling = false)
            .padding(horizontal = 32.dp, vertical = 24.dp)
    )
    {
        MyDreamBlock(dream)
        Spacer(modifier = Modifier.height(18.dp))

        Text(
            text = "Оранжерeя",
            style = MaterialTheme.typography.h2,
            color = MaterialTheme.colors.surface
        )

        Spacer(modifier = Modifier.height(12.dp))

        if (localdata.dreamList != null)
            for (dream in localdata.dreamList!!) {
                PlantCard(dream, context)
            }
        else Text(
            text = "Здесь будут растения, которые вы вырастите путем осуществления своих мечт",
            style = MaterialTheme.typography.body1,
            color = MaterialTheme.colors.onSurface,
            textAlign = TextAlign.Left,
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 8.dp)
        )

    }
}

