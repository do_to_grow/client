package com.example.myapplication.profile

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.myapplication.data.UserDream
import com.example.myapplication.data.localdata
import com.example.myapplication.setting.EditMainDataModel

@Composable
fun MyDreamBlock(
    dream: UserDream,
    model: ProfileModel = viewModel()
){

    Text(
        text = "Моя мечта",
        style = MaterialTheme.typography.h2,
        color = MaterialTheme.colors.surface,
        textAlign = TextAlign.Left
    )

    Spacer(modifier = Modifier.height(8.dp))
    Text(
        text = dream.dream.value,
        style = MaterialTheme.typography.h3,
        color = MaterialTheme.colors.primaryVariant,
        textAlign = TextAlign.Left,
        modifier = Modifier
            .padding(start = 8.dp)
    )

    Spacer(modifier = Modifier.height(4.dp))
    Text(
        text = model.support.value,
        style = MaterialTheme.typography.body2,
        color = MaterialTheme.colors.surface,
        modifier = Modifier.padding(start = 10.dp),
        textAlign = TextAlign.Left,
    )
}

