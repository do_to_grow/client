package com.example.myapplication.profile

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.example.myapplication.data.OldDream
import com.example.myapplication.data.localdata
import java.io.FileInputStream
import java.io.IOException


@Composable
fun PlantCard(
    dream: OldDream,
    context: Context
) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier.fillMaxSize()
            .border(1.dp, MaterialTheme.colors.onError, MaterialTheme.shapes.medium)
            .padding(8.dp)
    ) {
        var image: Bitmap =
            Bitmap.createBitmap(200, 300, Bitmap.Config.ARGB_8888)

        try {
            val fin: FileInputStream =
                context.openFileInput(dream.picture)
            image = BitmapFactory.decodeStream(fin)
            fin.close()

        } catch (e: IOException) {
            e.printStackTrace()
        }

        Image(
            bitmap = Bitmap.createScaledBitmap(
                image,
                ((image.width) * (localdata.SettingState.plantScale.value + 0.3f)).toInt(),
                ((image.height) * (localdata.SettingState.plantScale.value + 0.3f)).toInt(),
                true
            ).asImageBitmap(),
            contentDescription = "Описание",
            modifier = Modifier.width(400.dp)
        )
    }
    Spacer(modifier = Modifier.height(4.dp))
    Text(
        text =dream.dream,
        style = MaterialTheme.typography.h3,
        color = MaterialTheme.colors.surface,
        textAlign = TextAlign.Center,
        modifier = Modifier.fillMaxWidth()
    )

    Text(
        text = dream.motive,
        style = MaterialTheme.typography.body1,
        color = MaterialTheme.colors.onSurface,
        textAlign = TextAlign.Center,
        modifier = Modifier
            .fillMaxWidth()
    )
    Spacer(modifier = Modifier.height(16.dp))
}

