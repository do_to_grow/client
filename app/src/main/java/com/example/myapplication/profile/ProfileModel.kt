package com.example.myapplication.profile

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import kotlin.random.Random


class ProfileModel(): ViewModel() {


    var support: MutableState<String> = mutableStateOf("")

    init{

        support.value = getSupport()
    }


    fun getSupport(): String {

        val random = Random.nextInt(0, 100)

        val string = when (random) {
            in 0..30 -> "Стоит всех усилий"
            in 31..50 -> "Обязательно сбудется"
            in 51..70 -> "Достойна уважения"
            in 71..80 -> "Важна, но и отдых важен, чтобы были силы идти дальше"
            in 81..90 -> "Мы верим в вас!"
            in 91..100 -> "Вы хорошо справляетесь! Вспомните с чего вы начинали"
            else -> "Стоит всех усилий"
        }


        return string
    }

}