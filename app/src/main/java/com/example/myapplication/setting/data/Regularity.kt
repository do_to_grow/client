package com.example.myapplication.setting.data

enum class Regularity(val countDays: Int, val title: String) {
    None(0, "Не выбрано"),
    OneToOne(1, "Каждый день"),
    OneToWeek(7,"Раз в неделю"),
    OneTo2Week(14,"Раз в 2 недели"),
    OneTo3Week(21,"Раз в 3 недели"),
    OneToMonth(30,"Раз в месяц"),
    OneTo3Month(90,"Раз в 3 месяца"),
    OneTo6Month(180,"Раз в полгода"),
    OneToYear(365,"Раз в год"),
}