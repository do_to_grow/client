package com.example.myapplication.setting

import android.content.Context
import android.content.Intent
import androidx.compose.foundation.layout.*
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.myapplication.generalUnit.*
import com.example.myapplication.list.DialogWindow
import com.example.myapplication.registration.FirstStep


@Composable
fun EditMainData(
    context: Context,
    model: EditMainDataModel = viewModel(),
    FinishActivity: ()->Unit
) {

    val uiState by model.edit.collectAsState()


    DialogWindow(alertFlag = uiState.alertFlagNewDream.value,
        description = "Это действие нельзя будет отменить. Ваша текущая мечта будет потеряна навсегда. \nВы готовы бросить мечту?",
        onDismissRequest = {
            uiState.alertFlagNewDream.value = false
        },
        buttons = {
            filledButton(text = "НЕ ГОТОВ") {
                uiState.alertFlagNewDream.value = false
            }
            filledButton("  ГОТОВ  ") {
                uiState.alertFlagNewDream.value = false
                model.newDream()

                //перейти в активити регистрации
                val nav = Intent(context, FirstStep::class.java)
                context.startActivity(nav)
                //закончить это активити
                FinishActivity()
            }
        })

    Column(
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxWidth().padding(horizontal = 8.dp)
    )
    {

        Text(
            text = "Мечта и мотивация",
            style = MaterialTheme.typography.h2,
            color = MaterialTheme.colors.surface,
            textAlign = TextAlign.Left,
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(4.dp))
        Text(
            text = "Вы можете изменить заголовок вашей мечты или описание мотивации",
            style = MaterialTheme.typography.body1,
            color = MaterialTheme.colors.onSurface
        )
        Spacer(modifier = Modifier.height(12.dp))
        Column(modifier = Modifier.fillMaxWidth()) {
            Text(
                text = "Ваша мечта",
                style = MaterialTheme.typography.body2,
                color = MaterialTheme.colors.onSurface
            )
            textField(
                valu = uiState.dream.value, text = "Ваша мечта",
                onValue = { newText ->
                    if (newText.length < 21) {
                        uiState.flag.value = true

                        uiState.dream.value = newText
                    }
                },
                icon = {},
                modifier = Modifier.fillMaxWidth()
            )
        }
        Spacer(modifier = Modifier.height(12.dp))
        Column(modifier = Modifier.fillMaxWidth()) {
            Text(
                text = "Ваша мотивация",
                style = MaterialTheme.typography.body2,
                color = MaterialTheme.colors.onSurface
            )
            textField(
                uiState.motive.value, "Ваша мотивация",
                onValue = { newText ->
                    uiState.flag.value = true
                    uiState.motive.value = newText
                },
                icon = {},
                modifier = Modifier.fillMaxWidth()
            )
        }
        Spacer(modifier = Modifier.height(16.dp))
        if (uiState.flag.value) {
            filledButton(text = "Сохранить") {
                model.save()
            }
        } else {
            TextBotton("Начать новую мечту") {
                uiState.alertFlagNewDream.value = true
            }
        }
        Spacer(modifier = Modifier.height(16.dp))


        Text(
            text = "Растение",
            style = MaterialTheme.typography.h2,
            color = MaterialTheme.colors.surface,
            textAlign = TextAlign.Left,
            modifier = Modifier.fillMaxWidth()
        )
        Row(
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(
                text = "Размер растения",
                style = MaterialTheme.typography.body1,
                color = MaterialTheme.colors.surface,
                textAlign = TextAlign.Left,
            )


            Row(
                horizontalArrangement = Arrangement.End,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.fillMaxWidth().offset(x = 15.dp)
            ) {


                IconButton(
                    onClick = { uiState.plantScale.value -= 0.1f },
                    enabled = (uiState.plantScale.value > 0.6f),
                    //modifier = Modifier.fillMaxWidth().weight(0.05f)
                ) {
                    BackToArray(uiState.plantScale.value > 0.6f)
                }

                Text(
                    text = (((uiState.plantScale.value * 10).toInt()).toDouble() / 10).toString(),
                    style = MaterialTheme.typography.body1,
                    color = MaterialTheme.colors.surface,
                    textAlign = TextAlign.Center,
//                                modifier = Modifier.fillMaxWidth()
                )

                IconButton(
                    onClick = { uiState.plantScale.value += 0.1f },
                    enabled = (uiState.plantScale.value < 1.5f),
                    //modifier = Modifier.fillMaxWidth().weight(0.05f)
                ) {
                    ForwardToArray((uiState.plantScale.value < 1.5f))
                }
            }
        }

    }


}