package com.example.myapplication.setting.data

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf

data class RiminderBlock(
    var alertFlagReminder : MutableState<Boolean> = mutableStateOf(false),
    var reminder: MutableState<Boolean> = mutableStateOf(false),
    var localRegularity: Regularity = Regularity.None,
    val choice: List<Regularity> =  listOf(
        Regularity.OneToOne,
        Regularity.OneToWeek,
        Regularity.OneTo2Week,
        Regularity.OneTo3Week,
        Regularity.OneToMonth,
        Regularity.OneTo3Month,
        Regularity.OneTo6Month,
        Regularity.OneToYear
    ),
    var regularity: MutableState<Regularity> = mutableStateOf(localRegularity)
)