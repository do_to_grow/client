package com.example.myapplication.setting.data

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class JsonSettingState(
    @SerialName("plantScale")
    val plantScale: Float = 1.0f,
    @SerialName("theme")
    val theme: String = "Violet",
    @SerialName("change")
    val change: Boolean = false
)