package com.example.myapplication.setting.data

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import com.example.myapplication.network.UserData
import com.example.myapplication.setting.data.JsonSettingState

data class SettingState(
    val theme: MutableState<String> = mutableStateOf("Violet"),

    val plantScale: MutableState<Float> = mutableStateOf(1.0f),
    var userData:  UserData? = null,
    var proAccessFlag: MutableState<Boolean> = mutableStateOf(false)
) {
    fun toJson(): JsonSettingState {
        return JsonSettingState(plantScale.value, theme.value, proAccessFlag.value)
    }

    constructor(param: JsonSettingState) : this() {
        plantScale.value = param.plantScale
        theme.value = param.theme
        proAccessFlag.value = param.change
    }
}

