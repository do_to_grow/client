package com.example.myapplication.setting

import android.content.Context
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.selection.selectable
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.myapplication.generalUnit.ForwardToArray
import com.example.myapplication.generalUnit.TextBotton
import com.example.myapplication.generalUnit.filledButton
import com.example.myapplication.list.DialogWindow


@Composable
fun Reminder(
    context: Context,
    model: EditMainDataModel = viewModel()
) {

    val uiState by model.reminder.collectAsState()


    DialogWindow(header = "Периодичность",
        alertFlag = uiState.alertFlagReminder.value,
        onDismissRequest = {
            uiState.alertFlagReminder.value = false
        },
        buttons = {
            filledButton(text = "ОТМЕНА") {
                uiState.alertFlagReminder.value = false
            }
            filledButton("   ОК   ") {
                uiState.alertFlagReminder.value = false
                model.saveRegularity(context)
            }
        },
        main = {
            Spacer(modifier = Modifier.height(8.dp))
            Column(
                verticalArrangement = Arrangement.Top,
            ) {
                uiState.choice.forEach { block ->
                    val selected = uiState.regularity.value == block

                    Text(
                        text = block.title,
                        style = MaterialTheme.typography.body1,
                        color = if (selected) MaterialTheme.colors.secondary else MaterialTheme.colors.onSurface,
                        modifier = Modifier.fillMaxWidth()
                            .padding(vertical = 8.dp, horizontal = 4.dp)
                            .selectable(
                                selected = selected,
                                onClick = { uiState.regularity.value = block }
                            ),
                        textAlign = TextAlign.Center
                    )

                }
            }
        }
    )


    Column(
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxWidth().padding(horizontal = 8.dp)
    ) {
        Text(
            text = "Приложение будет напоминать вам о вашей мотивации при входе в приложение.\n" +
                    "Для корректной работы нужен интернет при входе в приложение",
            style = MaterialTheme.typography.body1,
            color = MaterialTheme.colors.onSurface
        )
        Spacer(modifier = Modifier.height(8.dp))

        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(
                text = "Напоминать мне о мотивации",
                style = MaterialTheme.typography.body1,
                color = MaterialTheme.colors.surface,
                textAlign = TextAlign.Left,
            )
            Row(
                horizontalArrangement = Arrangement.End,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.fillMaxWidth().offset(x = 15.dp)
            ) {
                Checkbox(
                    checked = uiState.reminder.value,
                    onCheckedChange = { value -> model.setValueCheckBox(value, context)},
                    //enabled = !goal.state,
                    colors = CheckboxDefaults.colors(
                        checkedColor = MaterialTheme.colors.secondary,
                        uncheckedColor = MaterialTheme.colors.secondary,
                        checkmarkColor = MaterialTheme.colors.primary,
                        disabledColor = MaterialTheme.colors.onError
                    )
                )

            }
        }


        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(
                text = "Периодичность",
                style = MaterialTheme.typography.body1,
                color = if ( uiState.reminder.value) MaterialTheme.colors.surface else MaterialTheme.colors.onSurface,
                textAlign = TextAlign.Left,
            )
            Row(
                horizontalArrangement = Arrangement.End,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.fillMaxWidth().offset(x = 15.dp)
            ) {
                TextBotton(header = uiState.localRegularity.title, isActive = uiState.reminder.value) {
                    uiState.alertFlagReminder.value = true
                }
                IconButton(
                    onClick = {uiState.alertFlagReminder.value = true},
                    enabled = (uiState.reminder.value),
                ) {
                    ForwardToArray(uiState.reminder.value)
                }
            }
        }
    }
}