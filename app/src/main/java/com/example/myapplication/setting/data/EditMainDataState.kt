package com.example.myapplication.setting.data

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import com.example.myapplication.data.localdata

data class EditMainDataState(
    val dream: MutableState<String> = mutableStateOf(""),
    val motive: MutableState<String> = mutableStateOf(""),
    val flag: MutableState<Boolean> = mutableStateOf(false),
    var alertFlagNewDream : MutableState<Boolean> = mutableStateOf(false),
    val plantScale: MutableState<Float> = localdata.SettingState.plantScale
)