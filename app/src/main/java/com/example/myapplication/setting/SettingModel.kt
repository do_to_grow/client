package com.example.myapplication.setting
import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.graphics.Color
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myapplication.R
import com.example.myapplication.data.localdata
import com.example.myapplication.network.DreamApi
import com.example.myapplication.network.LoadState
import com.example.myapplication.network.UserData
import com.example.myapplication.setting.data.EditMainDataState
import com.example.myapplication.setting.data.ProBlock
import com.example.myapplication.setting.data.Regularity
import com.example.myapplication.setting.data.RiminderBlock
import com.example.myapplication.ui.theme.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException


class EditMainDataModel(): ViewModel()  {

    private val _editMainDataStateState = MutableStateFlow(EditMainDataState())
    val edit: StateFlow<EditMainDataState> = _editMainDataStateState.asStateFlow()

    private val _riminderBlockState = MutableStateFlow(RiminderBlock())
    val reminder: StateFlow<RiminderBlock> = _riminderBlockState.asStateFlow()

    private val _proBlockState = MutableStateFlow(ProBlock(selectedOption = mutableStateOf(getColor())))
    val pro: StateFlow<ProBlock> = _proBlockState.asStateFlow()

    var statePro: LoadState by mutableStateOf(LoadState.Loading)
        private set

    var stateInfo: LoadState by mutableStateOf(LoadState.Loading)
        private set

    init {
        _editMainDataStateState.value.dream.value = localdata.actualUserDream!!.dream.value
        _editMainDataStateState.value.motive.value = localdata.actualUserDream!!.motive.value

        if (!(localdata.userId != 0 && localdata.userId != null)) repeatUser()
        else {if (localdata.SettingState.userData== null) getInfo() else {
            val data = localdata.SettingState.userData
            _riminderBlockState.value.reminder.value = data!!.remender
            _riminderBlockState.value.localRegularity = findLocalRegularity(data.frequency)
            stateInfo = LoadState.Success
        }}

    }

    fun getId():String{
        if (localdata.userId != 0 && localdata.userId != null)
            return "Ваш id @${localdata.userId.toString()}"
        else return ""
    }

    private fun getColor(): Color {
       if (localdata.SettingState.theme.value == "Blue") return MenuBlue
        if (localdata.SettingState.theme.value == "Green") return MenuGreen
         return MenuViolet
    }

    fun save(){
        localdata.actualUserDream!!.dream.value = _editMainDataStateState.value.dream.value
        localdata.actualUserDream!!.motive.value = _editMainDataStateState.value.motive.value
        _editMainDataStateState.value.flag.value = false
    }

    fun newDream(){
        localdata.actualUserDream = null
    }

    fun saveRegularity(context: Context){
        _riminderBlockState.value.localRegularity =  _riminderBlockState.value.regularity.value
        setReminder(context)
    }

    fun setValueCheckBox(value: Boolean, context: Context){
        if (value){
            _riminderBlockState.value.reminder.value = true
        } else {
            _riminderBlockState.value.reminder.value = false
            _riminderBlockState.value.localRegularity =  Regularity.None
            setReminder(context)
        }
    }

    private fun findLocalRegularity(value: Int): Regularity {
        when (value) {
            1 -> return Regularity.OneToOne
            7 -> return Regularity.OneToWeek
            14 -> return Regularity.OneTo2Week
            21 -> return Regularity.OneTo3Week
            30 -> return Regularity.OneToMonth
            90 -> return Regularity.OneTo3Month
            180 -> return Regularity.OneTo6Month
            365 -> return Regularity.OneToYear
            else -> return Regularity.None
        }
    }


    private fun setReminder(context: Context) {
              viewModelScope.launch {
           try {
                DreamApi.retrofitService.sendRemender(localdata.userId!!,
                    _riminderBlockState.value.reminder.value,
                    _riminderBlockState.value.localRegularity.countDays)

               localdata.SettingState.userData = UserData(
                   localdata.SettingState.userData!!.possibility,
                   _riminderBlockState.value.reminder.value,
                   _riminderBlockState.value.localRegularity.countDays)

            } catch (e: IOException) {
               Toast.makeText(context, "Произошла ошибка при сохранении данных. Подключите интернет и повторите", Toast.LENGTH_LONG).show()
               _riminderBlockState.value.reminder.value = localdata.SettingState.userData!!.remender

            } catch (e: HttpException) {
               Toast.makeText(context, "Произошла ошибка при сохранении данных. Подключите интернет и повторите", Toast.LENGTH_LONG).show()
               _riminderBlockState.value.reminder.value = localdata.SettingState.userData!!.remender
           }
        }
    }

    fun repeatUser(){
        stateInfo = LoadState.Loading
        if (localdata.userId == 0 || localdata.userId == null) {
        viewModelScope.launch {
            stateInfo = try {
                localdata.userId = DreamApi.retrofitService.getUser()
                getInfo()
                LoadState.Loading
            } catch (e: IOException) {
                Log.i("UserId", "IOException")
                LoadState.Error
            } catch (e: HttpException) {
                Log.i("UserId", "HttpException")
                LoadState.Error
            }
        } } else
            getInfo()
    }

    private fun getInfo(){
        stateInfo = LoadState.Loading
        viewModelScope.launch {
            stateInfo =  try {
                val data = DreamApi.retrofitService.getInfo(localdata.userId!!)
                localdata.SettingState.userData = data
                _riminderBlockState.value.reminder.value = data.remender
                _riminderBlockState.value.localRegularity = findLocalRegularity(data.frequency)
                localdata.SettingState.proAccessFlag.value = data.possibility
                LoadState.Success
            } catch (e: IOException) {
              Log.i("userData", "IOException")
                LoadState.Error
            } catch (e: HttpException) {
              Log.i("userData", "HttpException")
                LoadState.Error
            }
        }
    }

    fun saveTheme(context: Context) {

        if (_proBlockState.value.selectedOption.value == MenuBlue) localdata.SettingState.theme.value = context.getString(
           R.string.BlueColorName)
        else if (_proBlockState.value.selectedOption.value == MenuGreen) localdata.SettingState.theme.value = context.getString(
           R.string.GrrenColorName)
        else localdata.SettingState.theme.value = context.getString(
        R.string.VioletColorName)
    }

    fun checkCode() {
        statePro = LoadState.Loading
        if (localdata.userId == 0 || localdata.userId == null) statePro = LoadState.Error
        else {
        viewModelScope.launch {
            statePro = try {
                val data = DreamApi.retrofitService.checkCode(localdata.userId!!, _proBlockState.value.codePro.value)
                localdata.SettingState.proAccessFlag.value = data
                LoadState.Success
            } catch (e: IOException) {
                Log.i("userData", "IOException")
                LoadState.Error
            } catch (e: HttpException) {
                Log.i("userData", "HttpException")
                LoadState.Error
            }
        }}

        _proBlockState.value.codeAlert.value = true
        _proBlockState.value.codeEnter.value = false
    }

}