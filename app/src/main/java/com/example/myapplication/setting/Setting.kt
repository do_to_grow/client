package com.example.myapplication.setting


import android.content.Context
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel


@Composable
fun Setting(
    context: Context,
    FinishActivity: ()->Unit
) {

    val model: EditMainDataModel = viewModel()

    Column(
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState(), reverseScrolling = false)
            .padding(bottom = 24.dp)
    )
    {
        Column( horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 32.dp)
                .padding(top = 24.dp)
        )
        {
            Text(
                text = "Актуальная мечта",
                style = MaterialTheme.typography.h2,
                color = MaterialTheme.colors.secondary,
                textAlign = TextAlign.Left,
                modifier = Modifier.fillMaxWidth()
            )
            Spacer(modifier = Modifier.height(8.dp))

            EditMainData(context = context, FinishActivity = FinishActivity)
            Spacer(modifier = Modifier.height(18.dp))

            LoadInthernet(context, model)
        }

        ProVersion(context)


        Spacer(modifier = Modifier.height(18.dp))
        Text(
            text = "Мы открыты к предложениям! Пишите нам: email arina61616@gmail.com\n" +
                    model.getId(),
            style = MaterialTheme.typography.body1,
            color = MaterialTheme.colors.onSurface,
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth().padding(horizontal = 32.dp)
        )
    }

}

