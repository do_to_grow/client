package com.example.myapplication.setting.data

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.graphics.Color
import com.example.myapplication.data.localdata
import com.example.myapplication.ui.theme.MenuBlue
import com.example.myapplication.ui.theme.MenuGreen
import com.example.myapplication.ui.theme.MenuViolet

data class ProBlock(
    val colors: List<Color> = listOf(MenuViolet, MenuBlue, MenuGreen),
    val selectedOption: MutableState<Color>,
    val proAccess: MutableState<Boolean> = localdata.SettingState.proAccessFlag,
    var codeEnter : MutableState<Boolean> = mutableStateOf(false),
    var codePro : MutableState<String> = mutableStateOf(""),
    var codeAlert : MutableState<Boolean> = mutableStateOf(false),
)