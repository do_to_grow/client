package com.example.myapplication.setting

import android.content.Context
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.selection.selectable
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.myapplication.generalUnit.IconLoad
import com.example.myapplication.generalUnit.filledButton
import com.example.myapplication.generalUnit.textField
import com.example.myapplication.list.DialogWindow
import com.example.myapplication.network.LoadState

@Composable
fun ProVersion(
    context: Context,
    model: EditMainDataModel = viewModel()
) {

    val uiState by model.pro.collectAsState()

    DialogWindow(alertFlag = uiState.codeAlert.value,
        main = {
            when (model.statePro) {
                is LoadState.Loading -> {
                    Row(
                        horizontalArrangement = Arrangement.Center,
                        verticalAlignment = Alignment.CenterVertically,
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        IconLoad()
                    }
                }
                is LoadState.Success -> {
                    Row(
                        horizontalArrangement = Arrangement.Center,
                        verticalAlignment = Alignment.CenterVertically,
                        modifier = Modifier.fillMaxSize()
                    ) {
                        if (uiState.proAccess.value) {
                            Text(
                                text = "Поздравляем с приобретением Pro!",
                                style = MaterialTheme.typography.body1,
                                color = MaterialTheme.colors.primary,
                                modifier = Modifier.fillMaxWidth(),
                                textAlign = TextAlign.Center
                            )
                        } else {
                            Text(
                                text = "Введен неверный код",
                                style = MaterialTheme.typography.body1,
                                color = MaterialTheme.colors.error,
                                modifier = Modifier.fillMaxWidth(),
                                textAlign = TextAlign.Center
                            )
                        }
                    }
                }
                is LoadState.Error -> {
                    Text(
                        text = "Произошла ошибка\n Проверьте Интернет соединение и повторите",
                        style = MaterialTheme.typography.body1,
                        color = MaterialTheme.colors.error,
                        modifier = Modifier.fillMaxWidth(),
                        textAlign = TextAlign.Center
                    )
                }
            }
        },
        onDismissRequest = {
            uiState.codeAlert.value = false
        },
        buttons = {
            filledButton(text = "ОК") {
                uiState.codeAlert.value = false
            }
        })

    Column(
        modifier = Modifier.fillMaxWidth()
            .background(if (uiState.proAccess.value) MaterialTheme.colors.background else MaterialTheme.colors.onError)
            .padding(horizontal = 32.dp)
            .padding(bottom = 18.dp)
    ) {
        Text(
            text = "Pro версия",
            style = MaterialTheme.typography.h2,
            color = if (uiState.proAccess.value) MaterialTheme.colors.secondary else MaterialTheme.colors.background,
            textAlign = TextAlign.Left,
            modifier = Modifier.fillMaxWidth()
        )

        Spacer(modifier = androidx.compose.ui.Modifier.height(8.dp))

        Row(
            horizontalArrangement = Arrangement.Center,
            modifier = Modifier.fillMaxWidth()
        ) {
            uiState.colors.forEach { color ->
                val selected = uiState.selectedOption.value == color
                Box(
                    androidx.compose.ui.Modifier.padding(10.dp)
                        .size(50.dp)
                        .background(color = color)
                        .selectable(
                            selected = selected,
                            onClick = {
                                if (uiState.proAccess.value) uiState.selectedOption.value =
                                    color else {
                                }
                            }
                        )
                        .border(
                            width = if (selected) {
                                2.dp
                            } else {
                                0.dp
                            },
                            color = Color.Black
                        )
                )
            }
        }
        Spacer(modifier = androidx.compose.ui.Modifier.height(8.dp))
        if (uiState.proAccess.value) {
            Row(
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.Top,
                modifier = Modifier.fillMaxWidth()
            ) {
                filledButton(text = "Сохранить") {
                    model.saveTheme(context)
                }
            }
        } else {
            Text(
                text = "Версия Pro открывает доступ к изменению цвета приложения\n Чтобы купить, обратитесь на почту arina61616@gmail.com для получения кода\n" +
                        "После введите код в поле ниже",
                style = MaterialTheme.typography.body1,
                color = MaterialTheme.colors.background,
                textAlign = TextAlign.Center
            )
            Spacer(modifier = androidx.compose.ui.Modifier.height(6.dp))
            textField(
                valu = uiState.codePro.value, text = "Полученный код",
                onValue = { newText ->
                    if (newText.length < 17) {
                        uiState.codeEnter.value = true

                        uiState.codePro.value = newText
                    }
                },
                icon = {},
                modifier = Modifier.fillMaxWidth()
            )

            Row(
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.fillMaxWidth()
            ) {

                Button(
                    onClick = {
                        model.checkCode()
                    },
                    enabled = uiState.codeEnter.value,
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = MaterialTheme.colors.background,
                        disabledBackgroundColor = MaterialTheme.colors.primary,
                    )
                ) {

                    Text(
                        text = "Активировать",
                        style = MaterialTheme.typography.body1,
                        color = if (uiState.codeEnter.value) MaterialTheme.colors.surface else MaterialTheme.colors.primaryVariant
                    )
                }
            }
        }
    }

}