package com.example.myapplication.setting

import android.content.Context
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.myapplication.generalUnit.IconLoad
import com.example.myapplication.generalUnit.TextBotton
import com.example.myapplication.network.LoadState

@Composable
fun LoadInthernet(
    context: Context,
    model: EditMainDataModel = viewModel()
) {
    when (model.stateInfo) {
        is LoadState.Loading -> {
            IconLoad()
        }
        is LoadState.Success -> {
            Text(
                text = "Напоминания о мотивации",
                style = MaterialTheme.typography.h2,
                color = MaterialTheme.colors.secondary,
                textAlign = TextAlign.Left,
                modifier = Modifier.fillMaxWidth()
            )

            Spacer(modifier = Modifier.height(8.dp))

            Reminder(context)

            Spacer(modifier = Modifier.height(18.dp))

        }
        is LoadState.Error -> {
            Text(
                text = "Произошла ошибка D:\n Для последующих функций нужен Интернет",
                style = MaterialTheme.typography.body1,
                color = MaterialTheme.colors.error,
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Center
            )

            Row( horizontalArrangement = Arrangement.Center, modifier = Modifier.fillMaxWidth()) {
                TextBotton(header = "Повторить") {
                    model.repeatUser()
                }
            }
        }
    }
}

